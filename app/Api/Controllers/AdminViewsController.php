<?php

namespace App\Api\Controllers;

use App\Http\Controllers\Controller;

class AdminViewsController extends Controller
{

  public function __construct()
  {
    $this->middleware('jwt.auth', ['except' => ['index', 'login', 'password_reset', 'password_change']]);
  }

  public function index() {
    return view('admin.app');
  }

  public function login() {
    return view('admin.login');
  }

  public function password_reset() {
    return view('admin.password-reset');
  }

  public function password_change() {
    return view('admin.password-change');
  }

  public function register() {
    return view('admin.register');
  }

  public function dashboard() {
    return view('admin.dashboard');
  }

  public function home() {
    return view('admin.home');
  }

  public function admin() {
    return view('admin.admin');
  }

  public function registration() {
    return view('admin.registration');
  }

}