<?php

namespace App\Api\Controllers;

use App\Http\Controllers\Controller;

class AdminController extends Controller {

  public function __construct()
  {
    $this->middleware('jwt.auth', ['except' => []]);
  }

}
