<?php

Route::auth();

Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
Route::get('admin', 'AppController@admin');

//API
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['namespace' => 'App\Api\Controllers'], function ($api) {
	// Login route
	$api->post('authenticate', 'AuthController@authenticate');
	$api->post('register', 'AuthController@register');
  $api->get('admin/views/login', 'AdminViewsController@login');
  $api->get('admin/views/password-reset', 'AdminViewsController@password_reset');
  $api->get('admin/views/password-change', 'AdminViewsController@password_change');
	// Shows! All routes in here are protected and thus need a valid token
	//$api->group( [ 'protected' => true, 'middleware' => 'jwt.refresh' ], function ($api) {
	$api->group( [ 'middleware' => 'jwt.auth' ], function ($api) {

	  //jwt
	  $api->get('users/me', 'AuthController@me');
		$api->get('validate_token', 'AuthController@validateToken');

    //resources
		$api->resource('shows', 'ShowsController');

    //admin
    $api->get('admin/views/dashboard', 'AdminViewsController@dashboard');
    $api->get('admin/views/home', 'AdminViewsController@home');
    $api->get('admin/views/register', 'AdminViewsController@register');
    $api->get('admin/views/admin', 'AdminViewsController@admin');
    $api->get('admin/views/registration', 'AdminViewsController@registration');

	});
});



