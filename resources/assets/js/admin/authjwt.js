
'use strict';

var authJWT = angular.module('authJWT', []);

authJWT.controller('AuthController', ['$scope','$auth', '$state', '$http', '$rootScope', '$localStorage', function AuthController($scope, $auth, $state, $http, $rootScope, $localStorage) {

    $scope.loginError = false;
    $scope.loginErrorText;
    $rootScope.authState = true;

    //check if logged in
    if($auth.isAuthenticated()) {
        $state.go('dashboard');
    }

    $scope.login = function() {

        var credentials = {
            email: $scope.email,
            password: $scope.password,
            provider: 'email'
        }

        $auth.login(credentials).then(function(response) {

            var token = JSON.stringify(response.data.token);

            localStorage.setItem('token', token);
            $rootScope.authenticated = true;
            $rootScope.token = response.data.token;
            $rootScope.authState = false;

            $state.go('dashboard');
            
        }, function(error) {
            $scope.loginError = true;
            $scope.loginErrorText = error.data.error;
        });
    }
}]);

authJWT.factory('Auth', ['$http', '$localStorage', '$auth', 'urls', function ($http, $localStorage, $auth, urls) {
    function urlBase64Decode(str) {
        var output = str.replace('-', '+').replace('_', '/');
        switch (output.length % 4) {
            case 0:
                break;
            case 2:
                output += '==';
                break;
            case 3:
                output += '=';
                break;
            default:
                throw 'Illegal base64url string!';
        }
        return window.atob(output);
    }

    function getClaimsFromToken() {
        var token = localStorage.token;
        var user = {};
        if (typeof token !== 'undefined') {
            var encoded = token.split('.')[1];
            user = JSON.parse(urlBase64Decode(encoded));
        }
        return user;
    }

    var tokenClaims = getClaimsFromToken();

    return {
        signup: function (data, success, error) {
            $http.post(urls.BASE + '/api/authenticate', data).success(success).error(error)
        },
        signin: function (data, success, error) {
            $http.post(urls.BASE + '/api/register', data).success(success).error(error)
        },
        logout: function (success) {
            tokenClaims = {};
            localStorage.removeItem('token');
            success();
        },
        getTokenClaims: function () {
            return tokenClaims;
        }
    };
}
]);

authJWT.factory('CONFIG',['$rootScope', function($rootScope) {

    return {
        API_BASE: $rootScope.settings.apiRoot
    }

}]);

authJWT.factory('urls',['CONFIG', function(CONFIG) {
    
    return {
        shows: CONFIG.API_BASE + "shows",
        profile: CONFIG.API_BASE + "users/me"
    };

}]);

authJWT.factory('API', ['$resource', '$http', 'urls', function ($resource, $http, urls) {

    return {
        Shows: $resource(urls.shows),
        Profile: $resource(urls.profile)
    };

}]);



