angular.module('MetronicApp').controller('DashboardController', function($rootScope, $scope, $http, $timeout, $auth, $state, API) {

    //Get User Profile
    $scope.getUser = function() {
      API.Profile.get(function(response){
        $scope.user = response.user;
      });
    };

    //Logout
    $scope.logout = function() {
      $auth.logout();
      $state.go('auth');
    };

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    //Auth
    if($auth.isAuthenticated()) {
      $scope.getUser();
      $state.go('admin');
    } else {
      $state.go('auth');
    }

});