/***
Metronic AngularJS App Main Script
***/

/* Metronic App */
var MetronicApp = angular.module("MetronicApp", [
    "ui.router", 
    "ui.bootstrap", 
    "oc.lazyLoad",
    "ngResource",
    "ngStorage",
    "ngSanitize",
    "satellizer",
    "authJWT"
]); 

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MetronicApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

//AngularJS v1.3.x workaround for old style controller declarition in HTML
MetronicApp.config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);

MetronicApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/

/* Setup global settings */
MetronicApp.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        assetsPath: 'metronic/assets',
        globalPath: 'metronic/assets/global',
        layoutPath: 'metronic/assets/layouts/layout',
        apiRoot: 'http://localhost:8000/api/'
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* Setup App Main Controller */
MetronicApp.controller('AppController', ['$scope', '$rootScope', '$state', '$auth', function($scope, $rootScope, $state, $auth) {

    $rootScope.authState = false;

    if($auth.isAuthenticated()) {
        $state.go('dashboard');
    } else {
        $state.go('auth');
    }

    $scope.$on('$viewContentLoaded', function() {
        //App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive
    });

}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial 
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
MetronicApp.controller('HeaderController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('SidebarController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar(); // init sidebar
    });
}]);

/* Setup Layout Part - Quick Sidebar */
MetronicApp.controller('QuickSidebarController', ['$scope', function($scope) {    
    $scope.$on('$includeContentLoaded', function() {
       setTimeout(function(){
            QuickSidebar.init(); // init quick sidebar        
        }, 2000)
    });
}]);

/* Setup Layout Part - Theme Panel */
MetronicApp.controller('ThemePanelController', ['$scope', function($scope) {    
    $scope.$on('$includeContentLoaded', function() {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
MetronicApp.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
MetronicApp.config(['$stateProvider', '$urlRouterProvider', '$authProvider', function($stateProvider, $urlRouterProvider, $authProvider) {

    // Satellizer configuration that specifies which API
    // route the JWT should be retrieved from
    $authProvider.loginUrl = '/api/authenticate';

    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/auth");

    $stateProvider
        //Login
        .state('auth', {
          url: '/auth',
          templateUrl: '/api/admin/views/login',
          controller: 'AuthController as auth',
          resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad.load({
                name: 'MetronicApp',
                insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                files: [
                  'css/admin/app.css',
                ]
              });
            }]
          }
        })
        .state('password-reset', {
          url: '/password-reset',
          templateUrl: '/api/admin/views/password-reset',
          controller: 'PasswordResetController as reset',
          resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad.load({
                name: 'MetronicApp',
                insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                files: [
                  'css/admin/app.css',
                  'js/admin/controllers/PasswordResetController.js',
                ]
              });
            }]
          }
        })
        .state('password-change', {
          url: '/password-change',
          templateUrl: '/api/admin/views/password-change',
          controller: 'PasswordChangeController as change',
          resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad.load({
                name: 'MetronicApp',
                insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                files: [
                  'css/admin/app.css',
                  'js/admin/controllers/PasswordChangeController.js',
                ]
              });
            }]
          }
        })
        // Dashboard
        .state('dashboard', {
            url: "/dashboard",
            templateUrl: "/api/admin/views/dashboard",
            data: {pageTitle: 'Admin Dashboard Template'},
            controller: "DashboardController as dashboard",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'css/admin/app.css',
                            'metronic/assets/global/plugins/morris/morris.css',
                            'metronic/assets/global/plugins/morris/morris.min.js',
                            'metronic/assets/global/plugins/morris/raphael-min.js',
                            'metronic/assets/global/plugins/jquery.sparkline.min.js',

                            'metronic/assets/pages/scripts/dashboard.min.js',
                            'js/admin/controllers/DashboardController.js',
                        ] 
                    });
                }]
            }
        })
        .state('home', {
            url: '/home',
            templateUrl: '/api/admin/views/home',
            controller: 'HomeController as home',
            parent: 'dashboard',
            resolve: {
              deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                  name: 'MetronicApp',
                  insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                  files: [
                    'js/admin/controllers/HomeController.js',
                  ]
                });
              }]
            }
        })
      .state('admin', {
          url: '/admin',
          templateUrl: '/api/admin/views/admin',
          controller: 'AdminController as admin',
          parent: 'dashboard',
          resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                name: 'MetronicApp',
                insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                files: [
                  'js/admin/controllers/AdminController.js',
                ]
              });
            }]
          }
      })
      .state('registration', {
        url: '/registration',
        templateUrl: '/api/admin/views/registration',
        controller: 'RegistrationController as registration',
        parent: 'dashboard',
        resolve: {
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: 'MetronicApp',
              insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                'js/admin/controllers/RegistrationController.js',
              ]
            });
          }]
        }
      })
      .state('users', {
          url: '/users',
          templateUrl: '/api/admin/views/users',
          controller: 'UsersController as users',
          parent: 'dashboard',
          resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                name: 'MetronicApp',
                insertBefore: '#ng_load_plugins_after', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                files: [
                  'js/admin/controllers/UsersController.js',
                  'js/admin/scripts/Users.js',
                ]
              });
            }]
          }
      })
      .state('content', {
          url: '/content',
          templateUrl: '/api/admin/views/content',
          controller: 'ContentController as content',
          parent: 'dashboard',
          resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                name: 'MetronicApp',
                insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                files: [
                  'js/admin/controllers/ContentController.js',
                  'js/admin/scripts/Content.js',
                ]
              });
            }]
          }
      })
}]);

/* Init global settings and run the app */
MetronicApp.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
}]);