<div id="wrapper" class="container-fluid fill login">

  <!-- BEGIN LOGO -->
  <div class="logo">
    <a href="index.html">
      <img src="images/login_logo.png" alt="" /> </a>
  </div>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form role="form">
      <row class="container text-center">
        <h4>Please insert your registered email in order to reset your password</h4>
      </row>
      <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        <span> Enter email. </span>
      </div>
      <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <div class="input-icon">
          <i class="fa fa-user"></i>
          <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="username" ng-model="email" /> </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-offset-4 col-md-8">
            <button class="btn green" ng-click="resetPassword()"> Submit </button>
          </div>
        </div>
      </div>
    </form>
    <!-- END LOGIN FORM -->
  </div>
  <!-- END LOGIN -->

</div>