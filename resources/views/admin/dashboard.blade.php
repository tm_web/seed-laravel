{{--<div class="page-header-fixed page-sidebar-closed-hide-logo page-on-load" ng-class="{'page-content-white': settings.layout.pageContentWhite,'page-container-bg-solid': settings.layout.pageBodySolid, 'page-sidebar-closed': settings.layout.pageSidebarClosed}">--}}

  <!-- BEGIN HEADER -->
  <div data-ng-include="'html/admin/header.html'" data-ng-controller="HeaderController" class="page-header navbar navbar-fixed-top"> </div>
  <!-- END HEADER -->
  <div class="clearfix"> </div>
  <!-- BEGIN CONTAINER -->
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div data-ng-include="'html/admin/sidebar.html'" data-ng-controller="SidebarController" class="page-sidebar-wrapper"> </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">
        <!-- BEGIN ACTUAL CONTENT -->
        <ui-view class="fade-in-up"></ui-view>
        <!-- END ACTUAL CONTENT -->
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <div data-ng-include="'html/admin/footer.html'" data-ng-controller="FooterController" class="page-footer"> </div>
  <!-- END FOOTER -->

