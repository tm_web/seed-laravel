<div id="wrapper" class="container-fluid fill login">

  <!-- BEGIN LOGO -->
  <div class="logo">
    <a href="index.html">
      <img src="images/login_logo.png" alt="" /> </a>
  </div>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form role="form">
      <row class="container text-center">
        <h3>Change Password</h3>
      </row>
      <div class="form-group">
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Current Password"  ng-model="currentPassword" />
      </div>
      <div class="form-group">
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="New Password"  ng-model="password" />
      </div>
      <div class="form-group">
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Confirm New Password"  ng-model="confirmationPassword" />
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-offset-3 col-md-8">
            <button class="btn green"> Cancel </button>
            <button class="btn green" ng-click="changePassword()"> Submit </button>
          </div>
        </div>
      </div>
    </form>
    <!-- END LOGIN FORM -->
  </div>
  <!-- END LOGIN -->

</div>