<!-- BEGIN PAGE HEADER-->
<!-- END PAGE HEADER-->
<!-- BEGIN MAIN CONTENT -->
<div class="row" ng-controller="AdminController">
  <div class="content col-md-4 col-md-offset-4">
    <form role="form">
      <row class="container text-center">
        <h3>Create Account</h3>
      </row>
      <div class="form-group">
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="First Name"  ng-model="first_name" />
      </div>
      <div class="form-group">
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Last Name"  ng-model="last_name" />
      </div>
      <div class="form-group">
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email"  ng-model="email" />
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-offset-3 col-md-8">
            <button class="btn green"> CANCEL </button>
            <button class="btn green" ng-click="register()"> CREATE </button>
          </div>
        </div>
      </div>
    </form>
  </div>
  <!-- END MAIN CONTENT -->
  <!-- BEGIN MAIN JS & CSS -->
  <script>
  </script>
  <!-- BEGIN MAIN JS & CSS -->
