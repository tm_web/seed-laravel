<!-- BEGIN PAGE HEADER-->
<!-- END PAGE HEADER-->
<!-- BEGIN MAIN CONTENT -->
<div class="row" ng-controller="AdminController">
  <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light bordered">
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            <div class="col-md-12">
              <div class="btn-group pull-right">
                <button id="sample_editable_1_new" class="btn sbold green" ui-sref="registration"> CREATE AN ACCOUNT
                </button>
              </div>
            </div>
          </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
          <thead>
          <tr>
            <th class="center"> FIRST NAME </th>
            <th class="center"> LAST NAME </th>
            <th class="center"> EMAIL ID </th>
            <th class="center"> ACTION </th>
          </tr>
          </thead>
          <tbody>
          <tr class="odd gradeX">
            <td class="center"> Tony </td>
            <td class="center"> Stark </td>
            <td class="center"> tony@stark.com </td>
            <td class="center">
              <div class="actions">
                <a class="btn btn-icon-only btn-default" href="javascript:;">
                  <i class="icon-trash"></i>
                </a>
              </div>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
  </div>
<!-- END MAIN CONTENT -->
<!-- BEGIN MAIN JS & CSS -->
<script>
Admin.init();
</script>
<!-- BEGIN MAIN JS & CSS -->
